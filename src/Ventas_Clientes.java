
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Ventas_Clientes extends javax.swing.JFrame {

    public Ventas_Clientes() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro");
        mostrar();
        
        
    }
    
    void mostrar() {
    Tabla_Clientes.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
   
};     
    
        model.addColumn("Codigo");
        model.addColumn("Descripcion");
        model.addColumn("Direccion");
        model.addColumn("RIF/CI");
        model.addColumn("Telefono");
        model.addColumn("Fax");
        model.addColumn("Correo");
        model.addColumn("Website");
        model.addColumn("Fecha de Nacimiento");
       
       
        
         Tabla_Clientes.setModel(model);
      
        
        String []datos = new String [9];
        try {
            File f = new File("RegistroClientes.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"-");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Tabla_Clientes.setModel(model);
        } catch (Exception e) {
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        agregar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Tabla_Clientes = new javax.swing.JTable();
        volver = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario2.png"))); // NOI18N
        agregar.setBorder(null);
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 50, 80, 70));

        Tabla_Clientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(Tabla_Clientes);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 860, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, -1));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 30, 30));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(847, 0, -1, 30));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Lista de Clientes");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 880, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    
    
    public static String extraerDatos(){
        
        int i =Tabla_Clientes.getSelectedRow(); 
        String clientes=null;
        String descripcion=null;
        boolean band=false;
        
            if(i==-1){ 
               
            }else{
                String codigo=(String)Tabla_Clientes.getValueAt(i,0); 
                descripcion=(String)Tabla_Clientes.getValueAt(i,1); 
                String direccion=(String)Tabla_Clientes.getValueAt(i, 2);
                String rif_ci=(String)Tabla_Clientes.getValueAt(i, 3); 
                String telf=(String)Tabla_Clientes.getValueAt(i, 4); 
                String fax=(String)Tabla_Clientes.getValueAt(i, 5); 
                String email=(String)Tabla_Clientes.getValueAt(i, 6); 
                String website=(String)Tabla_Clientes.getValueAt(i, 7);
                String fecha_nacimiento=(String)Tabla_Clientes.getValueAt(i, 8);
                
                clientes=codigo+"-"+descripcion+"-"+direccion+"-"+rif_ci+"-"+telf+"-"+fax+"-"+email+"-"+website+"-"+fecha_nacimiento;
                band=true;
                
        } 
            if(band==true){
            return descripcion;
            }else{
                return null;
            }
    }
      
    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
       String nombre=extraerDatos();
        
        if(nombre==null){
            JOptionPane.showMessageDialog(null,"Seleccione una fila");
        }else{
            //extraerNombre(nombre);
            //Ventas_Vender ve_ve=new Ventas_Vender();
            this.setVisible(false);
            //ve_ve.setVisible(true);
            Ventas_Vender.cliente.setText(nombre);
            }   
    }//GEN-LAST:event_agregarActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Ventas_Vender admin= new Ventas_Vender();
        admin.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_volverActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){
        }
    }//GEN-LAST:event_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable Tabla_Clientes;
    private javax.swing.JButton agregar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton salir;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
