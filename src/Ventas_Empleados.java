
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Ventas_Empleados extends javax.swing.JFrame {

    public Ventas_Empleados() 
    {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro");
        mostrar();
        
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        
        
        
    }
    
    void mostrar() {
    Tabla_Empleados.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
};     
    
        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Apellido Paterno");
        model.addColumn("Apellido Materno");
        model.addColumn("Sexo");
        model.addColumn("Estado Civil");
        model.addColumn("Telefono");
        model.addColumn("Nacionalidad");
        model.addColumn("Lugar de Nacimiento");
        model.addColumn("Cedula de Identidad");
        model.addColumn("Correo");
        model.addColumn("Fecha de nacimiento");
        
       
       
        
       
        
         Tabla_Empleados.setModel(model);
      
        
        String []datos = new String [11];
        try {
            File f = new File("RegistroEmpleados.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"-");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Tabla_Empleados.setModel(model);
        } catch (Exception e) {
        }
    }
   public static String extraerDatos(){
        
        int i =Tabla_Empleados.getSelectedRow(); 
        String Empleados=null;
        String Nombre=null;
        boolean band=false;
        
            if(i==-1){ 
               
            }else{
                String codigo=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i,0); 
                Nombre=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i,1); 
                String ApellidoM=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 2);
                String ApellidoP=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 3); 
                String Sexo=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 4); 
                String EstadoCivil=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 5); 
                String Telefono=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 6); 
                String Nacionalidad=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 7);
                String Lugar=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 8);
                String Ci=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 9);
                String Correo=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 10);
                String Fecha=(String)Ventas_Empleados.Tabla_Empleados.getValueAt(i, 11);
                
                Empleados= codigo+"-"+Nombre+"-"+ApellidoM+"-"+ApellidoP+"-"+Sexo+"-"+EstadoCivil+"-"+Telefono+"-"+Nacionalidad+"-"+Lugar+"-"+Ci+"-"+Correo+"-"+Fecha;
                band=true;
                
        } 
            if(band==true){
            return Nombre;
            }else{
                return null;
            }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        volver = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla_Empleados = new javax.swing.JTable();
        agregar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Lista de Empleados");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 30));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 30, 30));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 0, 30, 30));

        Tabla_Empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(Tabla_Empleados);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 860, 420));

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-usuario.png"))); // NOI18N
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 50, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 880, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Ventas_Vender admin= new Ventas_Vender();
        this.setVisible(false);
        admin.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){
        }
    }//GEN-LAST:event_salirActionPerformed

    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        String nombre=extraerDatos();
        
        if(nombre==null){
            JOptionPane.showMessageDialog(null,"Seleccione una fila");
        }else{
            //extraerNombre(nombre);
            //Ventas_Vender ve_ve=new Ventas_Vender();
            this.setVisible(false);
            //ve_ve.setVisible(true);
            Ventas_Vender.empleado.setText(nombre);
            }   
    }//GEN-LAST:event_agregarActionPerformed

    public static String extraerNombre(String nombre){
        return nombre;
    }
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro().setVisible(true);
            }
        });
    }
 String codigo,nombre, ApellidoP, ApellidoM, Ci, Correo, Nacionalidad, EstadoCivil, Lugar, Sexo, Telefono, fecha;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable Tabla_Empleados;
    private javax.swing.JButton agregar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton salir;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
    String usu,tc;
    String sql;
}
