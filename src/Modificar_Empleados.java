import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import javax.swing.JOptionPane;


public class Modificar_Empleados extends javax.swing.JFrame {

    /**
     * Creates new form Modificar_Empleados
     */
    public Modificar_Empleados() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro de Empleados");
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        volver.setToolTipText("Volver");
        modificar.setToolTipText("Agregar Cliente");
        Limpiar.setToolTipText("Limpiar los campos de texto");
    }

   public static  void modificarFichero(File FficheroNuevo, File FficheroAntiguo, String Satigualinea,String Snuevalinea){        
        try {
            if(FficheroAntiguo.exists()){
                BufferedReader Flee= new BufferedReader(new FileReader(FficheroAntiguo));
                String Slinea;
                while((Slinea=Flee.readLine())!=null) { 
                    if (Slinea.toUpperCase().trim().equals(Satigualinea.toUpperCase().trim())) {
                        EcribirFichero(FficheroNuevo,Snuevalinea);
                    }else{
                         EcribirFichero(FficheroNuevo,Slinea);
                    }             
                }

                String SnomAntiguo=FficheroAntiguo.getName();
                Flee.close();

                borrarFichero(FficheroAntiguo);

                File file=new File(SnomAntiguo);
                FficheroNuevo.renameTo(file);
                
            }else{
            }
        } catch (Exception ex) {

        }
    }
   
    
    public static void borrarFichero(File Ffichero){
        try {
         if(Ffichero.exists()){
           Ffichero.delete(); 
         }
     } catch (Exception ex) {
     }
 } 
    
    public static void EcribirFichero(File Ffichero,String SCadena){
  try {
           if(!Ffichero.exists()){
               Ffichero.createNewFile();
           }
          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
          Fescribe.write(SCadena + "\r\n");
          Fescribe.close();
       } catch (Exception ex) {
       } 
    }
    
   public static void extraerDatos(int i){
        
        i = Registro.Tabla_Empleados.getSelectedRow();
        boolean band=false;
        
            if(i==-1){ 
                JOptionPane.showMessageDialog(null,"Seleccione una fila");
            }else{
                String codigo=(String)Registro.Tabla_Empleados.getValueAt(i,0); 
                String Nombre=(String)Registro.Tabla_Empleados.getValueAt(i,1); 
                String ApellidoM=(String)Registro.Tabla_Empleados.getValueAt(i, 2);
                String ApellidoP=(String)Registro.Tabla_Empleados.getValueAt(i, 3); 
                String Sexo=(String)Registro.Tabla_Empleados.getValueAt(i, 4); 
                String EstadoCivil=(String)Registro.Tabla_Empleados.getValueAt(i, 5); 
                String Telefono=(String)Registro.Tabla_Empleados.getValueAt(i, 6); 
                String Nacionalidad=(String)Registro.Tabla_Empleados.getValueAt(i, 7);
                String Lugar=(String)Registro.Tabla_Empleados.getValueAt(i, 8);
                String Ci=(String)Registro.Tabla_Empleados.getValueAt(i, 9);
                String Correo=(String)Registro.Tabla_Empleados.getValueAt(i, 10);
                String Fecha=(String)Registro.Tabla_Empleados.getValueAt(i, 11);
                
               Empleados= codigo+"-"+Nombre+"-"+ApellidoM+"-"+ApellidoP+"-"+Sexo+"-"+EstadoCivil+"-"+Telefono+"-"+Nacionalidad+"-"+Lugar+"-"+Ci+"-"+Correo+"-"+Fecha;
               
               Cod.setText(codigo); 
               Nombre_.setText(Nombre);
               ApellidoM_.setText(ApellidoM);
               ApellidoP_.setText(ApellidoP);
               Nacionalidad_.setText(Nacionalidad);
               LugarNacimiento_.setText(Lugar);
               CI_.setText(Ci);
               Sexo_.setText(Sexo);
               Telefono_.setText(Telefono);
               EstadoCivil_.setText(EstadoCivil);
               Correo_.setText(Correo);
               FechaNacimiento_.setText(Fecha);
               
        } 
    }

   
    
    void cambio(){
       nombre = Nombre_.getText();
       ApellidoM= ApellidoM_.getText();
       ApellidoP=ApellidoP_.getText();
       Sexo=Sexo_.getText();
       EstadoCivil=EstadoCivil_.getText();
       Ci = CI_.getText();
       Correo= Correo_.getText();
       Nacionalidad=Nacionalidad_.getText();
       Lugar=LugarNacimiento_.getText();
       Telefono=Telefono_.getText();
       Fecha = FechaNacimiento_.getText();
       codigo = Cod.getText();
       
       
       
       
   }
   void Limpiar(){
       Nombre_.setText("");
       ApellidoM_.setText("");
       ApellidoP_.setText("");
       Nacionalidad_.setText("");
       LugarNacimiento_.setText("");
       CI_.setText("");
       Sexo_.setText("");
       Telefono_.setText("");
       EstadoCivil_.setText("");
       Correo_.setText("");
       FechaNacimiento_.setText("");
       
       
       
   }
  void Validar(){
        cambio();
        boolean[] campo = new boolean[11];
        campo[0]=Nombre_.getText().isEmpty();
        campo[1]=ApellidoM_.getText().isEmpty();
        campo[2]=ApellidoP_.getText().isEmpty();
        campo[3]=Sexo_.getText().isEmpty();
        campo[4]=EstadoCivil_.getText().isEmpty();
        campo[5]=Telefono_.getText().isEmpty();
        campo[6]=Nacionalidad_.getText().isEmpty();
        campo[7]=LugarNacimiento_.getText().isEmpty();
        campo[8]=CI_.getText().isEmpty();
        campo[9]=Correo_.getText().isEmpty();
        campo[10]=FechaNacimiento_.getText().isEmpty();
       
       
        campo[10]=FechaNacimiento_.getText().isEmpty();
            if(campo[0] || campo[1] || campo[2] || campo[3] || campo[4]|| campo[5]|| campo[6]|| campo[7]|| campo[8]|| campo[9] || campo[10]){
                JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");

            }else{
                cambio();
                String Empleadoss = codigo+"-"+nombre+"-"+ApellidoM+"-"+ApellidoP+"-"+Sexo+"-"+EstadoCivil+"-"+Telefono+"-"+Nacionalidad+"-"+Lugar+"-"+Ci+"-"+Correo+"-"+Fecha;
                File file=new File("RegistroEmpleados.txt");
                File file_aux=new File("RegistroEmpleados2.txt");
                modificarFichero(file_aux, file, Empleados,Empleadoss);
                JOptionPane.showMessageDialog(null, "Registrado Satisfactoriamente");
                Registro reg = new Registro();
                reg.setVisible(true);
                dispose();
       
                Limpiar();
           
        
    }
    
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        Nombre_ = new javax.swing.JTextField();
        ApellidoP_ = new javax.swing.JTextField();
        ApellidoM_ = new javax.swing.JTextField();
        Sexo_ = new javax.swing.JTextField();
        EstadoCivil_ = new javax.swing.JTextField();
        Nacionalidad_ = new javax.swing.JTextField();
        Telefono_ = new javax.swing.JTextField();
        LugarNacimiento_ = new javax.swing.JTextField();
        CI_ = new javax.swing.JTextField();
        FechaNacimiento_ = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        Codigo = new javax.swing.JLabel();
        Limpiar = new javax.swing.JButton();
        volver = new javax.swing.JButton();
        Cod = new javax.swing.JTextField();
        Correo_ = new javax.swing.JTextField();
        modificar = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nombre(s)");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Apellido Materno");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Apellido Paterno");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Sexo");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, -1, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Estado Civil");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Telefono");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, -1, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Nacionalidad");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 220, -1, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Lugar de Nacimiento");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 250, -1, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Cedula de Identidad");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 280, -1, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Correo Electronico");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 310, -1, 20));

        Nombre_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nombre_ActionPerformed(evt);
            }
        });
        getContentPane().add(Nombre_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, 91, -1));
        getContentPane().add(ApellidoP_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, 91, -1));
        getContentPane().add(ApellidoM_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 280, 91, -1));
        getContentPane().add(Sexo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, 91, -1));
        getContentPane().add(EstadoCivil_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 340, 91, -1));
        getContentPane().add(Nacionalidad_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 220, 116, -1));
        getContentPane().add(Telefono_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 370, 91, -1));
        getContentPane().add(LugarNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 250, 116, -1));
        getContentPane().add(CI_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 280, 116, -1));
        getContentPane().add(FechaNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 340, 116, -1));

        jLabel11.setFont(new java.awt.Font("Caviar Dreams", 0, 18)); // NOI18N
        jLabel11.setText("Registro de Empleado");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Fecha de Nacimiento");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, -1, 20));

        Codigo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Codigo.setForeground(new java.awt.Color(102, 102, 102));
        getContentPane().add(Codigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 340, 20, 10));

        Limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        Limpiar.setBorderPainted(false);
        Limpiar.setContentAreaFilled(false);
        Limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(Limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 450, 40, 40));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 30));

        Cod.setEditable(false);
        Cod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CodActionPerformed(evt);
            }
        });
        getContentPane().add(Cod, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, 40, -1));
        getContentPane().add(Correo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, 116, -1));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 440, 60, 60));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Codigo");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, -1, 20));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 550, 540));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Nombre_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nombre_ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Nombre_ActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_LimpiarActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Registro reg=new Registro();
        reg.setVisible(true);
        dispose();

    }//GEN-LAST:event_volverActionPerformed

    private void CodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CodActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
        Validar();
    }//GEN-LAST:event_modificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Modificar_Empleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Modificar_Empleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Modificar_Empleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Modificar_Empleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Modificar_Empleados().setVisible(true);
            }
        });
    }
    static String Empleados=null;
    String codigo,nombre, ApellidoM, ApellidoP, Sexo, EstadoCivil, Telefono, Nacionalidad, Lugar, Ci, Correo, Fecha;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField ApellidoM_;
    public static javax.swing.JTextField ApellidoP_;
    public static javax.swing.JTextField CI_;
    public static javax.swing.JTextField Cod;
    private javax.swing.JLabel Codigo;
    public static javax.swing.JTextField Correo_;
    public static javax.swing.JTextField EstadoCivil_;
    public static javax.swing.JTextField FechaNacimiento_;
    private javax.swing.JButton Limpiar;
    public static javax.swing.JTextField LugarNacimiento_;
    public static javax.swing.JTextField Nacionalidad_;
    public static javax.swing.JTextField Nombre_;
    public static javax.swing.JTextField Sexo_;
    public static javax.swing.JTextField Telefono_;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton modificar;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
