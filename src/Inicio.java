
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Paths;

public class Inicio extends javax.swing.JFrame {
private int aux = 0;
private boolean realizado = false;

/*Contructor para barra progreso*/
hilo ejecutar = new hilo();

    public Inicio() {
        initComponents();
        Inicio.this.getRootPane().setOpaque(false);
        Inicio.this.getContentPane().setBackground(new Color (0,0,0,0));
        Inicio.this.setBackground(new Color (0,0,0,0));
        setLocationRelativeTo(null);
        setResizable(false);
        ingresoInicio();
        idEmpleados();
        idInventario();
        idclientes();
        idCompras();
    }
    
    
    /*Metodo para que al darle el boton iniciar me cree el usuario basico estandar
    Master - 1234*/
    void ingresoInicio(){
        try {
            String usuario = "master";
            String contra= new String("1234");
            FileWriter fw;
            BufferedWriter bw;
            File f = new File("Login.txt");
                 if(f.exists()){
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(usuario+"-"+contra);
                    }else{
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(usuario+"-"+contra);
            
            }
                     bw.close();
                     fw.close();
            
        } catch (Exception e) {
          System.out.println(e);
        }
    }
    void idInventario(){
       try {
            String id = "1";
            FileWriter fw;
            BufferedWriter bw;
            File f = new File("id.txt");
                 if(f.exists()){
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
                    }else{
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
            
            }
                     bw.close();
                     fw.close();
            
        } catch (Exception e) {
          System.out.println(e);
        } 
    }
    void idclientes(){
      try {
            String id = "1";
            FileWriter fw;
            BufferedWriter bw;
            File f = new File("IdClientes.txt");
                 if(f.exists()){
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
                    }else{
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
            
            }
                     bw.close();
                     fw.close();
            
        } catch (Exception e) {
          System.out.println(e);
        }   
    }
    void idEmpleados(){
        try {
            String id = "1";
            FileWriter fw;
            BufferedWriter bw;
            File f = new File("IdEmpleados.txt");
                 if(f.exists()){
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
                    }else{
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
            
            }
                     bw.close();
                     fw.close();
            
        } catch (Exception e) {
          System.out.println(e);
        } 
    }
    void idCompras(){
        try {
            String id = "1";
            FileWriter fw;
            BufferedWriter bw;
            File f = new File("IdCompras.txt");
                 if(f.exists()){
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
                    }else{
                     fw = new FileWriter(f);
                     bw = new BufferedWriter(fw);
                     bw.write(id);
            
            }
                     bw.close();
                     fw.close();
            
        } catch (Exception e) {
          System.out.println(e);
        }
        
        
    }
    
    
    /*Clase hilo para codigo de barra*/
    private class hilo extends Thread{
    @Override
        public void run(){
         try {while(true){
            aux++;
            barra.setValue(aux);
            repaint();
                switch(aux){
                    case 10:
                    texto.setText("Iniciando...");
                     break;
                    case 30:
                    texto.setText("Leyendo");
                      break; 
                    case 50:
                    texto.setText("Espere.");
                     break;
                    case 60:
                    texto.setText("Espere..");
                     break;
                    case 70:
                    texto.setText("Espere...");
                     break;
                    case 80:
                    texto.setText("Espere.");
                     break;
                    case 90:
                    texto.setText("Espere..");
                     break;
                    case 100:
                    texto.setText("Espere...");
                     break;    
                    case 110:
                    texto.setText("Finalizado");
                    InicioCuenta ini = new InicioCuenta();
                    ini.setVisible(true);
                    dispose();
                     break;
            }
            Thread.sleep(100);}
        
        } catch (Exception e) {
        }
    }
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barra = new javax.swing.JProgressBar();
        texto = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        barra.setBackground(new java.awt.Color(0, 0, 0));
        barra.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(barra, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 260, -1, -1));

        texto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        texto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(texto, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 210, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/saec.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, -90, -1, 555));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        /*Proceso para barra de carga*/
        if(realizado == false){
        realizado = true;
        barra.setMaximum(109);
        barra.setMinimum(0);
        barra.setStringPainted(true);
        
    
        ejecutar.start();
        }
    }//GEN-LAST:event_formWindowActivated

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel texto;
    // End of variables declaration//GEN-END:variables
}

