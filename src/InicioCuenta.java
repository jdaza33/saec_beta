
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.logging.Level;
import javax.swing.JOptionPane;


public class InicioCuenta extends javax.swing.JFrame {

   
    public InicioCuenta() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Inicio de sesion");
        
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        
    }
    
    
    /*Creando metodo para acceder al menu administrador*/
    void ingreso(String usuario, String contra){
       
        if(usuario.equals("master") && contra.equals("1234") ){
            Admin ad = new Admin();
            ad.setVisible(true);
            dispose();
        }else{
        JOptionPane.showMessageDialog(null,"Usuario incorrecto"); 
        }
        

    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        T_usu = new javax.swing.JTextField();
        T_contra = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        Inicio_S = new javax.swing.JButton();
        Icon = new javax.swing.JLabel();
        minimizar = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        Fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 18)); // NOI18N
        jLabel1.setText("Inicio de sesión");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Caviar Dreams", 1, 16)); // NOI18N
        jLabel2.setText("Usuario");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 230, -1, 30));

        T_usu.setBackground(new java.awt.Color(0, 0, 0));
        T_usu.setForeground(new java.awt.Color(255, 255, 255));
        T_usu.setBorder(null);
        T_usu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                T_usuActionPerformed(evt);
            }
        });
        T_usu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                T_usuKeyTyped(evt);
            }
        });
        getContentPane().add(T_usu, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 230, 140, 30));

        T_contra.setBackground(new java.awt.Color(0, 0, 0));
        T_contra.setForeground(new java.awt.Color(255, 255, 255));
        T_contra.setBorder(null);
        T_contra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                T_contraKeyTyped(evt);
            }
        });
        getContentPane().add(T_contra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 280, 140, 30));

        jLabel3.setFont(new java.awt.Font("Caviar Dreams", 1, 16)); // NOI18N
        jLabel3.setText("Contraseña");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 281, -1, 30));

        Inicio_S.setBackground(new java.awt.Color(255, 255, 255));
        Inicio_S.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Inicio_S.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/entrarminimi.png"))); // NOI18N
        Inicio_S.setToolTipText("");
        Inicio_S.setBorder(null);
        Inicio_S.setBorderPainted(false);
        Inicio_S.setContentAreaFilled(false);
        Inicio_S.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Inicio_S.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Inicio_SActionPerformed(evt);
            }
        });
        getContentPane().add(Inicio_S, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 320, 40, 40));

        Icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/usuario.png"))); // NOI18N
        getContentPane().add(Icon, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, -1, 130));

        minimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar.setBorderPainted(false);
        minimizar.setContentAreaFilled(false);
        minimizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizarActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 0, 30, 30));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, 30, 30));

        Fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fondo1.jpg"))); // NOI18N
        getContentPane().add(Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 390, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Inicio_SActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Inicio_SActionPerformed
        String usu = T_usu.getText();
        String pass = new String (T_contra.getPassword());
        /*Llamando el metodo Ingreso*/
        ingreso(usu, pass);
    }//GEN-LAST:event_Inicio_SActionPerformed

    private void T_usuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_T_usuActionPerformed
        
    }//GEN-LAST:event_T_usuActionPerformed

    private void T_usuKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_T_usuKeyTyped
        /*Proceso para tipear con enter*/
        char ctelaPresionada=evt.getKeyChar();
        
        if (ctelaPresionada==KeyEvent.VK_ENTER){
            Inicio_S.doClick();
        }
        
        
    }//GEN-LAST:event_T_usuKeyTyped

    private void T_contraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_T_contraKeyTyped
        /*Same process*/
        char ctelaPresionada=evt.getKeyChar();
        
        if (ctelaPresionada==KeyEvent.VK_ENTER){
            Inicio_S.doClick();
        }
    }//GEN-LAST:event_T_contraKeyTyped

    private void minimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizarActionPerformed
            this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizarActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salirActionPerformed

  
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioCuenta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Fondo;
    private javax.swing.JLabel Icon;
    private javax.swing.JButton Inicio_S;
    private javax.swing.JPasswordField T_contra;
    public static javax.swing.JTextField T_usu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton minimizar;
    private javax.swing.JButton salir;
    // End of variables declaration//GEN-END:variables

}
