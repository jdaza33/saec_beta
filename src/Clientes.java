
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Clientes extends javax.swing.JFrame {

    public Clientes() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        setTitle("Registro");
        mostrar();
        
        volver.setToolTipText("Volver");
        agregar.setToolTipText("Agregar Cliente");
        eliminar.setToolTipText("Eliminar Cliente Seleccionado");
        modificar.setToolTipText("Modificar Cliente Seleccionado");
        eliminar_todo.setToolTipText("Eliminar Todo");
    }
    
    void mostrar() {
    Tabla_Clientes.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
   
};     
    
        model.addColumn("Codigo");
        model.addColumn("Descripcion");
        model.addColumn("Direccion");
        model.addColumn("RIF/CI");
        model.addColumn("Telefono");
        model.addColumn("Fax");
        model.addColumn("Correo");
        model.addColumn("Website");
        model.addColumn("Fecha de Nacimiento");
       
       
        
         Tabla_Clientes.setModel(model);
      
        
        String []datos = new String [9];
        try {
            File f = new File("RegistroClientes.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"-");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Tabla_Clientes.setModel(model);
        } catch (Exception e) {
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        agregar = new javax.swing.JButton();
        eliminar = new javax.swing.JButton();
        modificar = new javax.swing.JButton();
        eliminar_todo = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Tabla_Clientes = new javax.swing.JTable();
        volver = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        agregar.setBorder(null);
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 40, 40));

        eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/quitar.png"))); // NOI18N
        eliminar.setBorder(null);
        eliminar.setBorderPainted(false);
        eliminar.setContentAreaFilled(false);
        eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 40, 40));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorder(null);
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 40, 40));

        eliminar_todo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesta.png"))); // NOI18N
        eliminar_todo.setBorder(null);
        eliminar_todo.setBorderPainted(false);
        eliminar_todo.setContentAreaFilled(false);
        eliminar_todo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar_todo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar_todoActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar_todo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 40, 40));

        Tabla_Clientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(Tabla_Clientes);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 860, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, -1));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 30, 30));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(847, 0, -1, 30));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Lista de Clientes");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 880, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public void remove(String file, String lineToRemove) {
 
    try {
 
      File inFile = new File(file);
      
      if (!inFile.isFile()) {
        System.out.println("Parameter is not an existing file");
        return;
      }
       
      //Construct the new file that will later be renamed to the original filename. 
      File tempFile = new File(inFile.getAbsolutePath() + ".tmp");
      
      BufferedReader br = new BufferedReader(new FileReader(file));
      PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
      
      String line = null;
 
      //Read from the original file and write to the new 
      //unless content matches data to be removed.
      while ((line = br.readLine()) != null) {
        
        if (!line.trim().equals(lineToRemove)) {
 
          pw.println(line);
          pw.flush();
        }
      }
      pw.close();
      br.close();
      
      //Delete the original file
      if (!inFile.delete()) {
        System.out.println("Could not delete file");
        return;
      } 
      
      //Rename the new file to the filename the original file had.
      if (!tempFile.renameTo(inFile))
        System.out.println("Could not rename file");
      
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
    
    public static String extraerDatos(){
        
        int i =Tabla_Clientes.getSelectedRow(); 
        String clientes=null;
        boolean band=false;
        
            if(i==-1){ 
               
            }else{
                String codigo=(String)Tabla_Clientes.getValueAt(i,0); 
                String descripcion=(String)Tabla_Clientes.getValueAt(i,1); 
                String direccion=(String)Tabla_Clientes.getValueAt(i, 2);
                String rif_ci=(String)Tabla_Clientes.getValueAt(i, 3); 
                String telf=(String)Tabla_Clientes.getValueAt(i, 4); 
                String fax=(String)Tabla_Clientes.getValueAt(i, 5); 
                String email=(String)Tabla_Clientes.getValueAt(i, 6); 
                String website=(String)Tabla_Clientes.getValueAt(i, 7);
                String fecha_nacimiento=(String)Tabla_Clientes.getValueAt(i, 8);
                
                clientes=codigo+"-"+descripcion+"-"+direccion+"-"+rif_ci+"-"+telf+"-"+fax+"-"+email+"-"+website+"-"+fecha_nacimiento;
                band=true;
                
        } 
            if(band==true){
            return clientes;
            }else{
                return null;
            }
    }
      public static  void borrarFichero(File Ffichero){
     try {
         if(Ffichero.exists()){
           BufferedWriter bw = new BufferedWriter(new FileWriter(Ffichero));
           bw.write("");
           bw.close();
         }
     } catch (Exception ex) {
     }
} 
     void Reseteardid(File Ffichero){
        try {
         if(Ffichero.exists()){
           BufferedWriter bw = new BufferedWriter(new FileWriter(Ffichero));
           bw.write("1");
           bw.close();
         }
     } catch (Exception ex) {
     }
        
    }
    
    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        RegistroClientes reg = new RegistroClientes();
        reg.setVisible(true);
        dispose();
    }//GEN-LAST:event_agregarActionPerformed

    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
            String clientes=extraerDatos();
            File file=new File("RegistroClientes.txt");
            
            if(clientes==null){
               JOptionPane.showMessageDialog(null,"Seleccione la fila que desea eliminar");
                }else{
                    int ax = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el cliente?");
                    if(ax == JOptionPane.YES_OPTION){
            
                    remove("RegistroClientes.txt",clientes);
            
                    //Eliminar fila de tabla, solo vista para el usuario
                    DefaultTableModel dtm = (DefaultTableModel) Tabla_Clientes.getModel(); //TableProducto es el nombre de mi tabla ;) 
                    dtm.removeRow(Tabla_Clientes.getSelectedRow()); 
            
                    JOptionPane.showMessageDialog(null, "Datos del Cliente eliminado con exito.");
                }else if(ax == JOptionPane.NO_OPTION){       
     }   
  }   
            
    }//GEN-LAST:event_eliminarActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
         String clientes=extraerDatos();
         if(clientes==null){
             JOptionPane.showMessageDialog(null,"Seleccione la fila que desea modificar");
         }else{
        Modificar_Cliente mc=new Modificar_Cliente();
        mc.setVisible(true);
        dispose();
        int i =Tabla_Clientes.getSelectedRow();
        Modificar_Cliente.extraerDatos(i);  
         }
        
        
    }//GEN-LAST:event_modificarActionPerformed

    private void eliminar_todoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar_todoActionPerformed
       
        File id = new File("idClientes.txt");
        File file=new File("RegistroClientes.txt");
        if (file.delete()){
        System.out.println("El fichero ha sido borrado satisfactoriamente");}
        else{
            System.out.println("No se ha podido eliminar");
        }
        Reseteardid(id);
        mostrar();
    }//GEN-LAST:event_eliminar_todoActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Admin admin= new Admin();
        admin.setVisible(true);
        dispose();
    }//GEN-LAST:event_volverActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){
        }
    }//GEN-LAST:event_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable Tabla_Clientes;
    private javax.swing.JButton agregar;
    private javax.swing.JButton eliminar;
    private javax.swing.JButton eliminar_todo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton modificar;
    private javax.swing.JButton salir;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
