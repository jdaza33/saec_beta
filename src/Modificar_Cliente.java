import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javax.swing.JOptionPane;
public class Modificar_Cliente extends javax.swing.JFrame {
    
static String clientes=null;
   
    public Modificar_Cliente() {
       initComponents();  
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro de Usuario");
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        fecha.setToolTipText("Opcional");
        WebSite.setToolTipText("Opcional");
        //Fin de Codigo para cambiar el icono
        
        volver.setToolTipText("Volver");
        modificar.setToolTipText("Agregar Cliente");
        Limpiar.setToolTipText("Limpiar los campos de texto");
    }
    
    
   void Limpiar(){
       Descripcion_.setText("");
       Direccion_.setText("");
       Rif_.setText("");
       Telefono_.setText("");
       Fax_.setText("");
       Email_.setText("");
       Web_.setText("");
       FechaNacimiento_.setText("");
       //Cod.setText(leerID());
   }
   
   public static  void modificarFichero(File FficheroNuevo, File FficheroAntiguo, String Satigualinea,String Snuevalinea){        
        try {
            if(FficheroAntiguo.exists()){
                BufferedReader Flee= new BufferedReader(new FileReader(FficheroAntiguo));
                String Slinea;
                while((Slinea=Flee.readLine())!=null) { 
                    if (Slinea.toUpperCase().trim().equals(Satigualinea.toUpperCase().trim())) {
                        EcribirFichero(FficheroNuevo,Snuevalinea);
                    }else{
                         EcribirFichero(FficheroNuevo,Slinea);
                    }             
                }

                String SnomAntiguo=FficheroAntiguo.getName();
                Flee.close();

                borrarFichero(FficheroAntiguo);

                File file=new File(SnomAntiguo);
                FficheroNuevo.renameTo(file);
                
            }else{
            }
        } catch (Exception ex) {

        }
    }
   
    
    public static void borrarFichero(File Ffichero){
        try {
         if(Ffichero.exists()){
           Ffichero.delete(); 
         }
     } catch (Exception ex) {
     }
 } 
    
    public static void EcribirFichero(File Ffichero,String SCadena){
  try {
           if(!Ffichero.exists()){
               Ffichero.createNewFile();
           }
          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
          Fescribe.write(SCadena + "\r\n");
          Fescribe.close();
       } catch (Exception ex) {
       } 
    }
  

   public static void extraerDatos(int i){
        
        i =Clientes.Tabla_Clientes.getSelectedRow(); 
        boolean band=false;
        
            if(i==-1){ 
                //JOptionPane.showMessageDialog(null,"Seleccione una fila");
            }else{
                String codigo=(String)Clientes.Tabla_Clientes.getValueAt(i,0); 
                String descripcion=(String)Clientes.Tabla_Clientes.getValueAt(i,1); 
                String direccion=(String)Clientes.Tabla_Clientes.getValueAt(i, 2);
                String rif_ci=(String)Clientes.Tabla_Clientes.getValueAt(i, 3); 
                String telf=(String)Clientes.Tabla_Clientes.getValueAt(i, 4); 
                String fax=(String)Clientes.Tabla_Clientes.getValueAt(i, 5); 
                String email=(String)Clientes.Tabla_Clientes.getValueAt(i, 6); 
                String website=(String)Clientes.Tabla_Clientes.getValueAt(i, 7);
                String fecha_nacimiento=(String)Clientes.Tabla_Clientes.getValueAt(i, 8);
                
                clientes=codigo+"-"+descripcion+"-"+direccion+"-"+rif_ci+"-"+telf+"-"+fax+"-"+email+"-"+website+"-"+fecha_nacimiento;
                
                Descripcion_.setText(descripcion);
                Direccion_.setText(direccion);
                Email_.setText(email);
                Cod.setText(codigo);
                Rif_.setText(rif_ci);
                Telefono_.setText(telf);
                Fax_.setText(fax);
                FechaNacimiento_.setText(fecha_nacimiento);
                Web_.setText(website);
                //System.out.println(clientes);
        } 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Direccion_ = new javax.swing.JTextField();
        Descripcion_ = new javax.swing.JTextField();
        Rif_ = new javax.swing.JTextField();
        Telefono_ = new javax.swing.JTextField();
        Fax_ = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Email = new javax.swing.JLabel();
        WebSite = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        Cod = new javax.swing.JTextField();
        FechaNacimiento_ = new javax.swing.JTextField();
        Web_ = new javax.swing.JTextField();
        Email_ = new javax.swing.JTextField();
        volver = new javax.swing.JButton();
        Limpiar = new javax.swing.JButton();
        modificar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(Direccion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 420, -1));
        getContentPane().add(Descripcion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 420, -1));
        getContentPane().add(Rif_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 140, -1));
        getContentPane().add(Telefono_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 210, 140, -1));
        getContentPane().add(Fax_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 240, 140, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Fax");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Telefono");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("RIF/CI");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Direccion");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, 20));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Descripcion");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, 20));

        Email.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Email.setText("Email");
        getContentPane().add(Email, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 340, -1, 20));

        WebSite.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        WebSite.setText("Website");
        getContentPane().add(WebSite, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 370, -1, 20));

        fecha.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        fecha.setText("Fecha de Nacimiento");
        getContentPane().add(fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 400, -1, 20));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Codigo");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 20));

        Cod.setEditable(false);
        getContentPane().add(Cod, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 100, -1));
        getContentPane().add(FechaNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 400, 116, -1));
        getContentPane().add(Web_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 370, 116, -1));
        getContentPane().add(Email_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 340, 116, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 30));

        Limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        Limpiar.setBorderPainted(false);
        Limpiar.setContentAreaFilled(false);
        Limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(Limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 480, -1, -1));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 480, 60, 50));

        jLabel6.setFont(new java.awt.Font("Caviar Dreams", 1, 18)); // NOI18N
        jLabel6.setText("Modificar Cliente");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Modificar_Clientes.png"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, -1, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Clientes cl=new Clientes();
        cl.setVisible(true);
        dispose();
    }//GEN-LAST:event_volverActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_LimpiarActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
         if((Descripcion_.getText().length()==0) || (Direccion_.getText().length()==0) || (Email_.getText().length()==0) || (Rif_.getText().length()==0) || (Telefono_.getText().length()==0) || (Fax_.getText().length()==0) || (FechaNacimiento_.getText().length()==0)|| (Web_.getText().length()==0)|| (Cod.getText().length()==0)){
               JOptionPane.showMessageDialog(null, "Por favor llene todos los cuadros");
       }else{
           //Crear variables para guardar lo que hay en los jtextfield
        String descripcionn, emaill, cod, rif, telfn, faxx, webb, direccionn, fecha_nacimiento;
      
        descripcionn=Descripcion_.getText();
        direccionn=Direccion_.getText();
        emaill=Email.getText();
        fecha_nacimiento=FechaNacimiento_.getText();
        rif=Rif_.getText();
        telfn=Telefono_.getText();
        faxx=Fax_.getText();
        webb=Web_.getText();
        cod=Cod.getText();
        
        String clientess=cod+"-"+descripcionn+"-"+direccionn+"-"+rif+"-"+telfn+"-"+faxx+"-"+emaill+"-"+webb+"-"+fecha_nacimiento;
        //System.out.println(clientess);
        
        File file=new File("RegistroClientes.txt");
        File file_aux=new File("RegistroClientes2.txt");
        modificarFichero(file_aux,file,clientes,clientess);
        
        this.setVisible(false);
        Clientes cli=new Clientes();
        cli.setVisible(true);
       }
    }//GEN-LAST:event_modificarActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroClientes().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JTextField Cod;
    private static javax.swing.JTextField Descripcion_;
    private static javax.swing.JTextField Direccion_;
    private javax.swing.JLabel Email;
    private static javax.swing.JTextField Email_;
    private static javax.swing.JTextField Fax_;
    private static javax.swing.JTextField FechaNacimiento_;
    private javax.swing.JButton Limpiar;
    private static javax.swing.JTextField Rif_;
    private static javax.swing.JTextField Telefono_;
    private javax.swing.JLabel WebSite;
    private static javax.swing.JTextField Web_;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JButton modificar;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
