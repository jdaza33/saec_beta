
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class RegistroEmpleado extends javax.swing.JFrame {

    public RegistroEmpleado() {
        initComponents();  
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro de Usuario");
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        String id_text=leerID();
        Cod.setText(id_text);
        
        volver.setToolTipText("Volver");
        Agregar.setToolTipText("Agregar Producto");
        Limpiar.setToolTipText("Limpiar los campos de texto");
    }
    
    
    public static void crearID(int id_int){
        //Creamos el archivo
        File id=new File("idEmpleados.txt");
        
        //Inicializamos las variables
        String id_string=String.valueOf(id_int+1);
   
        
        try{
            FileWriter escribir=new FileWriter(id,true);
            escribir.write(id_string);
            escribir.close();
        }catch(Exception e){
            
        }
    }
    public static  String leerID(){
        File Ffichero=new File("idEmpleados.txt");
        String aux=null;
   try {
           /*Si existe el fichero*/
           if(Ffichero.exists()){
           /*Abre un flujo de lectura a el fichero*/
           BufferedReader Flee= new BufferedReader(new FileReader(Ffichero));
           String Slinea;
           /*Lee el fichero linea a linea hasta llegar a la ultima*/
           while((Slinea=Flee.readLine())!=null) {
           /*Imprime la linea leida*/    
           aux=Slinea;         
           }
           
           //Cierra
           Flee.close();
         }else{}
 } catch (Exception ex) {}
       
   return aux;
 }
    
    public static void id() throws IOException{
       File id=new File("idEmpleados.txt");
        try {
         /*Si existe el fichero*/
         if(id.exists()){
           /*Borra el fichero*/  
           id.delete(); 
         }
     } catch (Exception ex) {
         /*Captura un posible error y le imprime en pantalla*/ 
     }
 
    }
    
   public void Registrar(String Empleados){
        try {
        FileWriter fw;
        BufferedWriter bw;


        File f = new File("RegistroEmpleados.txt");

        if(f.exists()){

        fw = new FileWriter(f,true);
        bw = new BufferedWriter(fw);
        bw.newLine();
        bw.write(Empleados);
        
        }else{
        fw = new FileWriter(f);
        bw = new BufferedWriter(fw);
        bw.write(Empleados);
        }
        bw.close();
        fw.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    
    }
    
   void cambio(){
       nombre = Nombre_.getText();
       ApellidoM= ApellidoM_.getText();
       ApellidoP=ApellidoP_.getText();
       Sexo=Sexo_.getText();
       EstadoCivil=EstadoCivil_.getText();
       Ci = CI_.getText();
       Correo= Correo_.getText();
       Nacionalidad=Nacionalidad_.getText();
       Lugar=LugarNacimiento_.getText();
     
       Telefono=Telefono_.getText();
       
       Fecha = FechaNacimiento_.getText();
       codigo = leerID();
       
       
       
   }
   void Limpiar(){
       Nombre_.setText("");
       ApellidoM_.setText("");
       ApellidoP_.setText("");
       Nacionalidad_.setText("");
       LugarNacimiento_.setText("");
       CI_.setText("");
       Sexo_.setText("");
       Telefono_.setText("");
       EstadoCivil_.setText("");
       Correo_.setText("");
       FechaNacimiento_.setText("");
       Cod.setText(leerID());
       
       
   }
  void Validar(){
     cambio();
        boolean[] campo = new boolean[11];
        campo[0]=Nombre_.getText().isEmpty();
        campo[1]=ApellidoM_.getText().isEmpty();
        campo[2]=ApellidoP_.getText().isEmpty();
        campo[3]=Sexo_.getText().isEmpty();
        campo[4]=EstadoCivil_.getText().isEmpty();
        campo[5]=Telefono_.getText().isEmpty();
        campo[6]=Nacionalidad_.getText().isEmpty();
        campo[7]=LugarNacimiento_.getText().isEmpty();
        campo[8]=CI_.getText().isEmpty();
        campo[9]=Correo_.getText().isEmpty();
        campo[10]=FechaNacimiento_.getText().isEmpty();
       
       
        campo[10]=FechaNacimiento_.getText().isEmpty();
            if(campo[0] || campo[1] || campo[2] || campo[3] || campo[4]|| campo[5]|| campo[6]|| campo[7]|| campo[8]|| campo[9] || campo[10]){
                JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");

            }else{
                String Empleados = codigo+"-"+nombre+"-"+ApellidoM+"-"+ApellidoP+"-"+Sexo+"-"+EstadoCivil+"-"+Telefono+"-"+Nacionalidad+"-"+Lugar+"-"+Ci+"-"+Correo+"-"+Fecha;
                Registrar(Empleados);
                JOptionPane.showMessageDialog(null, "Registrado Satisfactoriamente");
                try {
                 id();
        } catch (IOException ex) {
           
        }
        
        int codigo_id= Integer.parseInt(codigo);
        crearID(codigo_id);
       
                Limpiar();
           
        
    }
    
}
  
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        Nombre_ = new javax.swing.JTextField();
        ApellidoP_ = new javax.swing.JTextField();
        ApellidoM_ = new javax.swing.JTextField();
        Sexo_ = new javax.swing.JTextField();
        EstadoCivil_ = new javax.swing.JTextField();
        Nacionalidad_ = new javax.swing.JTextField();
        Telefono_ = new javax.swing.JTextField();
        LugarNacimiento_ = new javax.swing.JTextField();
        CI_ = new javax.swing.JTextField();
        FechaNacimiento_ = new javax.swing.JTextField();
        Agregar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        Codigo = new javax.swing.JLabel();
        Limpiar = new javax.swing.JButton();
        volver = new javax.swing.JButton();
        Cod = new javax.swing.JTextField();
        Correo_ = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nombre(s)");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Apellido Materno");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Apellido Paterno");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Sexo");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, -1, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Estado Civil");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Telefono");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, -1, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Nacionalidad");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 220, -1, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Lugar de Nacimiento");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 250, -1, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Cedula de Identidad");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 280, -1, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Correo Electronico");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 310, -1, 20));

        Nombre_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nombre_ActionPerformed(evt);
            }
        });
        getContentPane().add(Nombre_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, 91, -1));
        getContentPane().add(ApellidoP_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, 91, -1));
        getContentPane().add(ApellidoM_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 280, 91, -1));
        getContentPane().add(Sexo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, 91, -1));
        getContentPane().add(EstadoCivil_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 340, 91, -1));
        getContentPane().add(Nacionalidad_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 220, 116, -1));
        getContentPane().add(Telefono_, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 370, 91, -1));
        getContentPane().add(LugarNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 250, 116, -1));
        getContentPane().add(CI_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 280, 116, -1));
        getContentPane().add(FechaNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 340, 116, -1));

        Agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        Agregar.setBorderPainted(false);
        Agregar.setContentAreaFilled(false);
        Agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });
        getContentPane().add(Agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 450, 40, 40));

        jLabel11.setFont(new java.awt.Font("Caviar Dreams", 0, 18)); // NOI18N
        jLabel11.setText("Registro de Empleado");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Fecha de Nacimiento");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, -1, 20));

        Codigo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Codigo.setForeground(new java.awt.Color(102, 102, 102));
        getContentPane().add(Codigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 340, 20, 10));

        Limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        Limpiar.setBorderPainted(false);
        Limpiar.setContentAreaFilled(false);
        Limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(Limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 450, 40, 40));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 30));

        Cod.setEditable(false);
        Cod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CodActionPerformed(evt);
            }
        });
        getContentPane().add(Cod, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, 40, -1));
        getContentPane().add(Correo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, 116, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Codigo");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, -1, 20));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hombre-de-negocios.png"))); // NOI18N
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 50, -1, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 550, 540));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed
     Validar();
     cambio();
     
     
       
        
    }//GEN-LAST:event_AgregarActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Registro reg=new Registro();
        reg.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_volverActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
    Limpiar();
    }//GEN-LAST:event_LimpiarActionPerformed

    private void CodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CodActionPerformed

    private void Nombre_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nombre_ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Nombre_ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroEmpleado().setVisible(true);
            }
        });
    }
    
    String codigo,nombre, ApellidoM, ApellidoP, Sexo, EstadoCivil, Telefono, Nacionalidad, Lugar, Ci, Correo, Fecha;
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JTextField ApellidoM_;
    private javax.swing.JTextField ApellidoP_;
    private javax.swing.JTextField CI_;
    private javax.swing.JTextField Cod;
    private javax.swing.JLabel Codigo;
    private javax.swing.JTextField Correo_;
    private javax.swing.JTextField EstadoCivil_;
    private javax.swing.JTextField FechaNacimiento_;
    private javax.swing.JButton Limpiar;
    private javax.swing.JTextField LugarNacimiento_;
    private javax.swing.JTextField Nacionalidad_;
    private javax.swing.JTextField Nombre_;
    private javax.swing.JTextField Sexo_;
    private javax.swing.JTextField Telefono_;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
