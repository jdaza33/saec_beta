
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JOptionPane;

public class Admin extends javax.swing.JFrame {

    public Admin() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Registro = new javax.swing.JLabel();
        empleados = new javax.swing.JButton();
        compras = new javax.swing.JButton();
        clientes = new javax.swing.JButton();
        ventas = new javax.swing.JButton();
        inventario = new javax.swing.JButton();
        Registro1 = new javax.swing.JLabel();
        Registro2 = new javax.swing.JLabel();
        Registro3 = new javax.swing.JLabel();
        Registro5 = new javax.swing.JLabel();
        salir = new javax.swing.JButton();
        minimizar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Registro.setFont(new java.awt.Font("Caviar Dreams", 0, 16)); // NOI18N
        Registro.setText("Compras");
        getContentPane().add(Registro, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 200, -1, -1));

        empleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/empleados.png"))); // NOI18N
        empleados.setBorder(null);
        empleados.setBorderPainted(false);
        empleados.setContentAreaFilled(false);
        empleados.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        empleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empleadosActionPerformed(evt);
            }
        });
        getContentPane().add(empleados, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 220, 130, 120));

        compras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/compras.png"))); // NOI18N
        compras.setBorder(null);
        compras.setContentAreaFilled(false);
        compras.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        compras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprasActionPerformed(evt);
            }
        });
        getContentPane().add(compras, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 220, 130, 120));

        clientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/clientes.png"))); // NOI18N
        clientes.setBorder(null);
        clientes.setContentAreaFilled(false);
        clientes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        clientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientesActionPerformed(evt);
            }
        });
        getContentPane().add(clientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 220, 130, 120));

        ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ventas.png"))); // NOI18N
        ventas.setBorder(null);
        ventas.setContentAreaFilled(false);
        ventas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ventasActionPerformed(evt);
            }
        });
        getContentPane().add(ventas, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 380, 130, 120));

        inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/inventario.png"))); // NOI18N
        inventario.setBorder(null);
        inventario.setContentAreaFilled(false);
        inventario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inventarioActionPerformed(evt);
            }
        });
        getContentPane().add(inventario, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 380, 130, 120));

        Registro1.setFont(new java.awt.Font("Caviar Dreams", 0, 16)); // NOI18N
        Registro1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Registro1.setText("Inventario");
        getContentPane().add(Registro1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 360, 110, -1));

        Registro2.setFont(new java.awt.Font("Caviar Dreams", 0, 16)); // NOI18N
        Registro2.setText("Clientes");
        getContentPane().add(Registro2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 200, -1, -1));

        Registro3.setFont(new java.awt.Font("Caviar Dreams", 0, 16)); // NOI18N
        Registro3.setText("Empleados");
        getContentPane().add(Registro3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 200, -1, -1));

        Registro5.setFont(new java.awt.Font("Caviar Dreams", 0, 16)); // NOI18N
        Registro5.setText("Ventas");
        getContentPane().add(Registro5, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 360, -1, -1));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, -1, 30));

        minimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar.setBorder(null);
        minimizar.setContentAreaFilled(false);
        minimizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizarActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, -1, 30));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/logo_grande.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 240, 140));

        jLabel2.setFont(new java.awt.Font("Caviar Dreams", 1, 36)); // NOI18N
        jLabel2.setText("¡Bienvenido!");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 50, -1, -1));

        Fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fondo2.jpg"))); // NOI18N
        getContentPane().add(Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void empleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empleadosActionPerformed
    this.setVisible(false);
    Registro reg = new Registro();
    reg.setVisible(true);
    }//GEN-LAST:event_empleadosActionPerformed

    private void comprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprasActionPerformed
     Compras c = new Compras();
     c.setVisible(true);
     dispose();
    }//GEN-LAST:event_comprasActionPerformed

    private void clientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientesActionPerformed
       this.setVisible(false);
       Clientes cl = new Clientes();
       cl.setVisible(true);
    }//GEN-LAST:event_clientesActionPerformed

    private void ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ventasActionPerformed
       this.setVisible(false);
       Ventas_Vender ven=new Ventas_Vender();
       ven.setVisible(true);
    }//GEN-LAST:event_ventasActionPerformed

    private void inventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inventarioActionPerformed
        Inventario in= new Inventario();
        this.setVisible(false);
        in.setVisible(true);
        
    }//GEN-LAST:event_inventarioActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
     int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){       
        }     
    }//GEN-LAST:event_salirActionPerformed

    private void minimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizarActionPerformed
            this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizarActionPerformed

    public static void main(String args[]) {
       java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Admin().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Fondo;
    private javax.swing.JLabel Registro;
    private javax.swing.JLabel Registro1;
    private javax.swing.JLabel Registro2;
    private javax.swing.JLabel Registro3;
    private javax.swing.JLabel Registro5;
    private javax.swing.JButton clientes;
    private javax.swing.JButton compras;
    private javax.swing.JButton empleados;
    private javax.swing.JButton inventario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton minimizar;
    private javax.swing.JButton salir;
    private javax.swing.JButton ventas;
    // End of variables declaration//GEN-END:variables
}
