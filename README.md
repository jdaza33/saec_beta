# Sistema Administrativo para la empresa SAEC

Sistesma administrativo de una empresa llamada SAEC. Este sistema cuenta con ventas, inventario, clientes,
compras, proveedores, etc. 

- Estado: Finalizado
- Lenguaje: Java SE
- Interfaz Grafica: Java Swing
- Base de datos: .txt
- IDE ó Editor: NetBeans 8.1
- Uso: No-Comercial

![Alt text](/Captures/proyecto_saec1.jpg)

![Alt text](/Captures/proyecto_saec2.jpg)

![Alt text](/Captures/proyecto_saec3.jpg)

![Alt text](/Captures/proyecto_saec4.jpg)

![Alt text](/Captures/proyecto_saec5.jpg)

![Alt text](/Captures/proyecto_saec6.jpg)
